[Перелік усіх робіт](README.md)

# Оптимізація коду. Використання функцій

## Мета роботи

Розглянути приклади роботи з функціями JavaScript. Навчитись створювати власні функції. Навчитися оптимізувати код за допомогою функцій

## Обладнання

Персональний комп'ютер. Текстовий редактор Sublime Text 3 або Notepad++. Пакет програм XAMPP. Web-браузер Chrome, Firefox, Opera, Internet Explorer

## Теоретичні відомості

Функция - это именованная последовательность операторов (инструкций). Любая функция имеет следующий синтаксис:
```
function имя (){

оператор;

.......

оператор;

}
```
Сначала идет ключевое слово function, затем имя функции, затем в круглых скобках перечисляются параметры (если они есть), затем в фигурных скобках перечисляются операторы, т.е. последовательность выполняемых действий. Каждый оператор заканчивается точкой с запятой.

## Хід роботи

1. Перевірити чи встановлено пакет програм web-розробника XAMPP
2. Викликати панель керування xampp-control.exe
3. Впевнитись, що web-сервер Apache запущений
4. Перейти за адресою http://127.0.0.1/ або http://localhost/ та впевнитись, що сторінка вітання XAMPP завантажилась
5. Очистити зміст каталогу C:\xampp\htdocs\
6. Створити файл index.html в середині каталогу C:\xampp\htdocs\
7. Перейти за адресою http://127.0.0.1/ або http://localhost/ та впевнитись, що сторінка, яку ви створили, завантажилася корректно
8. Створити файл з назвою script.js та помістити його поряд з файлом index.html
9. Підключити файл script.js в секції `<head>`
10. Відповідно до завдань нижче створити необхідні HTML-форми, призначити обробники подій для елементів форми. Обробники подій оформити у вигляді анонімною функції.
11. У файл script.js помістити код та перевірити його роботу.
12. Створити програму для обчислення коренів квадратного рівняння, за введенними коефіцієнтами. Якщо рівняння не має коренів, повідомити про це користувача. Дискримінант рівняння має обчислювати спеціальна функція.
13. Створити програму складання двох матриць 5х5. Створити функцію яка заповнить матриці довільними цілими числами від 0 до 99. Результат складання вивести в третю матрицю. Складання має виконувати окрема функція, аргументи функції мають бути двовимірними масивами.
14. Створити функцію, яка буде переміщувати кнопку в довільне місце сторінки після її натиснення
15. У "підвал" сторінки та файл script.js помістити інформацію про виконавця роботи: група, ПІБ, дата виконання.
16. Для кожного етапу роботи зробити знімки екрану або скопіювати текст консолі та додати їх у звіт з описом кожного скіншота
17. Зберегти звіт у форматі PDF
18. Роздрукувати звіт та письмово відповісти на контрольні запитання

## Контрольні питання

1. Що таке функція? Яке її призначення?
2. Що таке анонімна функція? Для чого можна її використовувати?
3. Які види аргументів може повертати функція?
4. Яким чином опрацьовуються у функції глобальні змінні?

## Довідники та додаткові матеріали

1. [Валідатор W3C](https://validator.w3.org)
2. [Современный учебник JavaScript](https://learn.javascript.ru)
3. [Справочник по HTML](http://htmlbook.ru)